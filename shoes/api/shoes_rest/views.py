from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer",
                  "name",
                  "color",
                  "picture_url",
                  "bin"
                  ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer",
                  "name",
                  "color",
                  "picture_url",
                  ]

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id = None):

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

def api_show_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
            shoe = Shoe.objects.get(pk=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
