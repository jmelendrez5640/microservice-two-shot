from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()



class Hat(models.Model):
    # id = models.PositiveSmallIntegerField(primary_key=True)
    fabric = models.CharField(max_length=30)
    style = models.CharField(max_length=30)
    color = models.CharField(max_length=15)
    picture = models.URLField(max_length=200)
    # location_in_wardrobe = models.CharField(max_length=100)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.PROTECT,
    )


    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.fabric} - {self.style}/{self.color}/{self.location_in_wardrobe}"

    class Meta:
        ordering = ("fabric", "style", "color")
