from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style",
        "color",
        "picture",
    ]
    encoders = {
        "locations": LocationVODetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"import_href": o.location.import_href}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "location",
        "fabric",
        "style",
        "color",
        "picture",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_VO_id=None):
    if request.method == "GET":
        if location_VO_id is not None:
            hat = Hat.objects.filter(location=location_VO_id)
        else:
            hat = Hat.objects.all()

        return JsonResponse(
            {"hats": hat},
            encoder=HatEncoder,
        )
    else:
        # location_href = f'/api/locations/{location_VO_id}'
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
                )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )



@require_http_methods(["GET","DELETE"])
def api_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        hat= Hat.objects.get(id=pk)
        count, _ = hat.delete()
        return JsonResponse({"deleted": count > 0})
        # except Hat.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response


    # if request.method == "GET":
    #     try:
    #         hat = Hat.objects.get(id=pk)
    #         return JsonResponse(
    #             hat,
    #             encoder=HatEncoder,
    #             safe=False
    #         )
    #     except Hat.DoesNotExist:
    #         response = JsonResponse({"message": "Does not exist"})
    #         response.status_code = 404
    #         return response

    # else:
    #     try:
    #         content = json.loads(request.body)
    #         hat = Hat.objects.get(id=pk)

    #         props = ["fabric", "style", "color", "picture"]
    #         for prop in props:
    #             if prop in content:
    #                 setattr(hat, prop, content[prop])
    #         hat.save()
    #         return JsonResponse(
    #             hat,
    #             encoder=HatEncoder,
    #             safe=False,
    #         )
    #     except Hat.DoesNotExist:
    #         response = JsonResponse({"message": "Does not exist"})
    #         response.status_code = 404
    #         return response



