from django.urls import path
from .views import api_hat, api_list_hats


urlpatterns = [
    path("hats/", api_list_hats, name="api_create_hats"),
    path("locations/<int:location_VO_id>/hats/", api_list_hats, name="api_list_hats_location"),
    path("hats/<int:pk>/", api_hat, name="api_show_hat"),
]
