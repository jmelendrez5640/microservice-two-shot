import React, {useEffect, useState } from 'react';


function HatForm() {

    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    async function fetchLocations() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }
      useEffect(() => {
        fetchLocations();
      }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
          fabric,
          style,
          color,
          picture,
          location,

        };



    const locationUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newHats = await response.json();
      console.log(newHats);
      setFabric('');
      setStyle('');
      setColor('');
      setPicture('');
      setLocation('');
    }
  }

  function handleChangeFabric(event) {
    const { value } = event.target;
    setFabric(value);
  }

  function handleChangeStyle(event) {
    const { value } = event.target;
    setStyle(value);
  }

  function handleChangeColor(event) {
    const { value } = event.target;
    setColor(value);
  }

  function handleChangePicture(event) {
    const { value } = event.target;
    setPicture(value);
  }

  function handleChangeLocation(event) {
    const { value } = event.target;
    setLocation(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create A New Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input value={fabric} onChange={handleChangeFabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input value={style} onChange={handleChangeStyle} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <label htmlFor="picture">Photo URL</label>
              <input value={picture} onChange={handleChangePicture} className="form-control" id="picture" name="picture"/>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleChangeLocation} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;


// {conferences.map(conference => {
//     return (
//       <option key={conference.href} value={conference.href}>{conference.name}</option>
//     )
//   })}
