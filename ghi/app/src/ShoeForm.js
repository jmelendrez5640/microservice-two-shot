import React, {useEffect, useState } from 'react';

function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    async function fetchBins() {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);

        }
    }

useEffect(() => {
    fetchBins();

  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
        manufacturer,
        name,
        color,
        picture_url: pictureUrl,
        bin,
    }

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };


    const response = await fetch(shoeUrl, fetchConfig)
    if (response.ok) {
        setManufacturer('');
        setName('');
        setColor('');
        setPictureUrl('');
        setBin('');

    }

  }

  function handleChangeManufacturer(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  function handleChangeName(event) {
    const { value } = event.target;
    setName(value);
  }
  function handleChangeColor(event) {
    const { value } = event.target;
    setColor(value);
  }
  function handleChangePictureUrl(event) {
    const { value } = event.target;
    setPictureUrl(value);
  }
  function handleChangeBin(event) {
    const { value } = event.target;
    setBin(value);
  }

  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">

            <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input value={name} onChange={handleChangeName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="Name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input value={pictureUrl} onChange={handleChangePictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">picture_url</label>
            </div>

            <div className="mb-3">
              <select value={bin} onChange={handleChangeBin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ShoeForm
