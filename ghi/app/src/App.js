import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatList from './HatList';
import HatForm from './HatForm';

function App(props) {
  const [ shoes, setShoes ] = useState([]);
  const [ hats, setHats ] = useState([]);

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes');

    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function loadHats() {
    const response = await fetch('http://localhost:8090/api/hats');
    const { hats } = await response.json();
    setHats(hats);
  }


  useEffect(() => {
    getShoes();
    loadHats();

  }, [])


  if (shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList shoes={shoes} />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path='/hats/new' element={<HatForm />}/>
          <Route path= '/hats' element={<HatList hats={hats}/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
